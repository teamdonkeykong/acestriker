﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMovement : MonoBehaviour {

	public float movementSpeed = 1.0f;
	public int invert = -1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 direction = new Vector3 (horizontal,invert*vertical, 0);
		Vector3 finalDirection = new Vector3 (horizontal, invert * vertical, 10.0f);

		transform.position += direction * movementSpeed * Time.deltaTime;
		transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (finalDirection), Mathf.Deg2Rad * 50.0f);
		//Stop player from going off screen
		//When Players reaches desired (L/R)possition make him stop
		if (transform.position.x <= -3.18f) {
			transform.position = new Vector3 (-3.18f, transform.position.y, transform.position.z);
		} 
		else if (transform.position.x >= 5.5f) {
			transform.position = new Vector3 (5.5f, transform.position.y, transform.position.z);
		} 
		//When Players reaches desired (U/D)possition make him stop
		if (transform.position.y <= -3.5f) {
			transform.position = new Vector3 (transform.position.x, -3.5f, transform.position.z);
		} 
		else if (transform.position.y >= 2.5f) {
			transform.position = new Vector3 (transform.position.x, 2.5f, transform.position.z);
		}
	}
}
	
