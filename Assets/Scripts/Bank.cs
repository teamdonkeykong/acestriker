﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bank : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Single axis(for windows/keyboard)
		Vector3 newRotationEuler = transform.rotation.eulerAngles;
		newRotationEuler.x = -90 * Input.GetAxis ("Bank");
		Quaternion newQuat = Quaternion.identity;
		newQuat.eulerAngles = newRotationEuler;
		transform.rotation = newQuat;
	}
}
