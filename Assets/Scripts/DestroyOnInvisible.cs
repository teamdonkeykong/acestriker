﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnInvisible : MonoBehaviour {
	public float delay = 1.0f;

	void Start(){
		Invoke ("DestroyNow", delay);
	}

	void DestroyNow(){
		Destroy (gameObject);
	}
}

