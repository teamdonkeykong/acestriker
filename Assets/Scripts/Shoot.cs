﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
	public Rigidbody bullet;
	public float velocity = 100.0f;
	void Update () {
		if (Input.GetButton("Fire1")) {
			//Rigidbody newBullet = Instantiate (bullet,transform.position, Quaternion.identity) as Rigidbody;
			Rigidbody newBullet = Instantiate (bullet,transform.position, transform.rotation) as Rigidbody;
			newBullet.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
		}
	}
}
